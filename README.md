# Serious game in Virtual Reality
*Made with Unity*

**Authors (ISIMA ZZ3 students):**
- Lilian Hertel
- Thibault Chatry
- Killian Zehnder
- Antoine Thirion

**Last updated:** 16/01/2023


## General description

This project is a serious game reproducing in virtual reality the experience of a technician in a tire manufacturing plant. The user has to put tires away in the right dumpster according to its characteristics, as fast as possible.


## Installation

All you have to do is clone this repository, then open the project in Unity. All the required plugins are already integrated in the project. You can either play with a VR headset or without. 
If you don't have a headset, please make sure you deactivated the Player game object and activated the _simpleTestPlayer instead. **Remark:** as the keyboard is designed to function in VR only, you will have to enter an empty name at the end of each game if you use the _simpleTestPlayer.


## How to play

To begin the game, the user has to click on the green button located on the command console to the left of the green and red dumpsters. A green light blinks to confirm that the button was clicked correctly. Immediately after clicking on the green button, 10 tires will appear in the yellow bin on the left, and a timer will start. The user can see the timer at any time on the screen above the command console. The game can be reset at any time by clicking on the red button.

During the game, the player has to scan each tire either with the scanner or the portable barcode reader. The tire can be correct, incorrect or not readable. If the tire is not readable, the user has to scan it with the portable barcode reader. If the tire is correct, the user has to put it away in the green dumpster. If the tire is incorrect, the user has to put it away in the red dumpster.

The game ends when the user has put away all the tires in the right bin. As the game stops, the user has to enter his name with the keyboard that appeared in front of them. The user can then see his score in the leaderboard on the screen above. He can restart the game by clicking on the red button and then on the green button to start a new one. The scores are memorized between games and are displayed on the leaderboard, sorted in ascending order.
