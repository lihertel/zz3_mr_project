using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attached to an empty GameObject. Handles buttons on the command center.
/// </summary>
public class ButtonsManager : MonoBehaviour
{
    /// <summary>
    /// This method is called when the green button is pressed.
    /// /// Tells the <see cref="GameManager"/> to start the game.
    /// </summary>
    public void SpawnTiresButtonHandler()
    {
        EventManager.TriggerEvent("StartGame", null);
    }

    /// <summary>
    /// This method is called when the red button is pressed.
    /// Tells the <see cref="GameManager"/> to reset the game.
    /// </summary>
    public void ResetTiresButtonHandler()
    {
        EventManager.TriggerEvent("ResetGame", null);
    }
}
