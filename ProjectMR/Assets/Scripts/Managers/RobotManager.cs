using UnityEngine;
using Valve.VR;

/// <summary>
/// Attached to an empty GameObject. Handles control of the robot arm.
/// </summary>
public class RobotManager : MonoBehaviour
{
    [SerializeField]
    private SteamVR_Input_Sources trackpad;

    [SerializeField]
    private SteamVR_Input_Sources handtype;

    [SerializeField]
    private SteamVR_Action_Vector2 BougerRobot;

    [SerializeField]
    private GameObject Player;

    [SerializeField]
    private GameObject _brasRobot;

    [SerializeField]
    private GameObject _baseRobot;

    [SerializeField]
    private float rotateSpeed = 1f;

    /// <summary>
    /// Gets the current value of the trackpad input.
    /// </summary>
    /// <returns>The curernt input</returns>
    private Vector2 GetPositionTrackpad()
    {
        return BougerRobot.GetAxis(trackpad);
    }

    /// <summary>
    /// Moves the robot arm according to input (left controller trackpad).
    /// </summary>
    void FixedUpdate()
    {
        _brasRobot.transform.Rotate(new Vector3(GetPositionTrackpad().y * rotateSpeed, 0.0f, 0f));
        _baseRobot.transform.Rotate(new Vector3(0f, GetPositionTrackpad().x * rotateSpeed, 0f));
    }
}
