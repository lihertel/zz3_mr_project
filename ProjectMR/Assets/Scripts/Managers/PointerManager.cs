using UnityEngine;
using Valve.VR.Extras;

/// <summary>
/// Attached to an empty GameObject. Handles the laser pointer that allows interacting with objects from a distance (click on buttons, grab objects).
/// </summary>
public class PointerManager : MonoBehaviour
{
    /// <summary>
    /// The LaserPointer component (attached to the right hand).
    /// </summary>
    [SerializeField] private SteamVR_LaserPointer laserPointer;

    /// <summary>
    /// Sets the method that will be called when an object is interacted with.
    /// </summary>
    void Awake()
    {
        laserPointer.PointerClick += PointerClick;
    }

    /// <summary>
    /// Called when an object is pointed and interacted with.
    /// Triggers an event depending on the nature of the object.
    /// </summary>
    /// <param name="sender">Unused</param>
    /// <param name="e">The object pointed by the laser.</param>
    public void PointerClick(object sender, PointerEventArgs e)
    {
        switch (e.target.tag)
        {
            case "Button":
                switch (e.target.name)
                {
                    case "OK":
                        EventManager.TriggerEvent("StartGame", null);
                        break;
                    case "KO":
                        EventManager.TriggerEvent("ResetGame", null);
                        break;
                }
                break;
            // Grabbable objects
            case "Tire":
            case "Scanner":
            case "Grabbable":
                EventParam p = new EventParam();
                p.sender = laserPointer.gameObject;
                p.receiver = e.target.gameObject;
                EventManager.TriggerEvent("Grab", p);
                break;
            // Keyboard tags
            case "Key":
                Debug.Log("key pressed");
                EventParamString c = new EventParamString();
                c.Value = e.target.name;
                EventManager.TriggerEvent("KeyPressed", c);
                break;

            case "Enter":
                EventManager.TriggerEvent("EnterPressed", null);
                break;

            case "Space":
                EventManager.TriggerEvent("SpacePressed", null);
                break;

            case "Return":
                EventManager.TriggerEvent("ReturnPressed", null);
                break;
        }
    }
}