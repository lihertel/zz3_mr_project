using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Attached to an empty GameObject. This is the main script that handles the game loop: 
/// - Starting the game: spawn tires, start the timer
/// - Resetting the game: despawn tires, stop the timer, make the keyboard visible (wait for user name) -> display time and name on the screen (<see cref="ResultsCanvas"/>).
/// Keeps track of the state of the current game : number of tires in each dumpster. When all tires are in the correct dumpster, end the game.
/// Keeps track of previous times and names.
/// </summary>
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject prefabTire;

    // Change this to configure how many tires will spawn at the beginning of a game.
    [Tooltip("Change this to configure how many tires will spawn at the beginning of a game.")]
    [SerializeField] private int nbTires = 10;

    // Keeps a referece to all tires to destroy them when necessary
    private List<GameObject> _tires;

    // State variables
    private bool _gameStarted = false;
    private bool _gameFinished = false;
    private int _countTiresConform = 0;
    private int _countTiresError = 0;

    private float _time = 0; // Timer of the current game
    private Dictionary<string, float> _gameTimes; // Stores previous names and times

    private Coroutine _startCoroutine = null;

    /// <summary>
    /// Initializes some attributes and subscribes to all the necessary events.
    /// </summary>
    void Start()
    {
        _tires = new List<GameObject>();
        _gameTimes = new Dictionary<string, float>();

        EventManager.StartListening("StartGame", OnStartGame);
        EventManager.StartListening("ResetGame", OnResetGame);
        EventManager.StartListening("EndGame", EndGame);

        EventManager.StartListening("TireInDumpsterConform", OnTireInDumpsterConform);
        EventManager.StartListening("TireOutDumpsterConform", OnTireOutDumpsterConform);
        EventManager.StartListening("TireInDumpsterError", OnTireInDumpsterError);
        EventManager.StartListening("TireOutDumpsterError", OnTireOutDumpsterError);
    }

    /// <summary>
    /// Triggered when the green button is pressed:
    /// - Triggers the animation of the green button (<see cref="GreenButton"/>) and the light (<see cref="FeedbackLights"/>).
    /// - If the game is not started, starts it (spawn tires)
    /// </summary>
    /// <param name="e"></param>
    private void OnStartGame(EventParam e)
    {
        EventManager.TriggerEvent("BlinkLightGreen", null);
        EventManager.TriggerEvent("AnimateGreenButton", null);
        if (_gameStarted) return; // Prevents spawning tires more than once
        if (_startCoroutine == null) _startCoroutine = StartCoroutine(SpawnTires());
    }

    /// <summary>
    /// Coroutine that spawns tires. there is a 0.5 seconds delay between each tire to prevent collisions (tires flying away).
    /// </summary>
    /// <returns>Current state of the coroutine</returns>
    private IEnumerator SpawnTires()
    {
        for (int i = 0; i < nbTires; i++)
        {
            GameObject tire = Instantiate(prefabTire, transform.position + Random.Range(-1, 1) * Vector3.forward + Random.Range(-1, 1) * Vector3.right, Quaternion.identity);
            _tires.Add(tire);
            yield return new WaitForSeconds(0.5f);
        }
        _gameStarted = true;
        _startCoroutine = null;
    }

    /// <summary>
    /// Updates the timer every frame while the game is not finished.
    /// When all the tires are in the correct dumpster: stops the timer and shows the keyboard to let the user type their name.
    /// </summary>
    private void Update()
    {
        // If game is still running, increment the timer and update the display with the new value.
        if (_gameStarted && !_gameFinished)
        {
            _time += Time.deltaTime;
            EventParamStringArray eventParams = new EventParamStringArray();
            string[] strings = { "", "", "Temps écoulé : " + _time, "", "" };
            eventParams.Strings = strings;
            EventManager.TriggerEvent("UpdateCanvas", eventParams);
        }

        // If all tires in the correct dumpster, finish the game.
        if (_gameStarted && (_countTiresConform + _countTiresError) == _tires.Count && !_gameFinished)
        {
            _gameFinished = true;
            EventManager.TriggerEvent("ShowKeyboard", null);
        }

    }

    /// <summary>
    /// Triggered after the user has typed their name, ends the game, and displays results on the screen.
    /// </summary>
    /// <param name="e">An EventParamString holdign the name of the player</param>
    private void EndGame(EventParam e)
    {
        // Retrieves the name
        EventParamString param = (EventParamString)e;
        string playerName = param.Value;

        // Adds a number after the name if this name already exists in previous games
        int countNameOccurences = _gameTimes.Where(t => t.Key.StartsWith(playerName)).Count();
        if (countNameOccurences > 0) playerName += countNameOccurences + 1;

        // Stores this game's result (player name + time) and sorts the results array by time (so that the best times are always on top of the screen).
        _gameTimes.Add(playerName, _time);
        _gameTimes = _gameTimes.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

        // Constructs the list of strings to display on the screen
        EventParamStringArray eventParams = new EventParamStringArray();
        List<string> list = _gameTimes.Select(k => k.Value.ToString() + "s : " + k.Key.ToString()).ToList();
        list.Insert(0, "Meilleurs scores :");
        eventParams.Strings = list.ToArray();

        // Triggers the event to update the screen
        EventManager.TriggerEvent("UpdateCanvas", eventParams);
    }

    /// <summary>
    /// When a conform tire is put on the right dumpster, increments the corresponding counter.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnTireInDumpsterConform(EventParam e)
    {
        _countTiresConform++;
    }

    /// <summary>
    /// When a conform tire is removed from the dumpster, decrement the corresponding counter.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnTireOutDumpsterConform(EventParam e)
    {
        _countTiresConform--;
    }

    /// <summary>
    /// When a non-conform tire is put on the right dumpster, increments the corresponding counter.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnTireInDumpsterError(EventParam e)
    {
        _countTiresError++;
    }

    /// <summary>
    /// When a non-conform tire is removed from the dumpster, decrement the corresponding counter.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnTireOutDumpsterError(EventParam e)
    {
        _countTiresError--;
    }

    /// <summary>
    /// Triggered when the game red button is pressed:
    /// - Triggers the animation of the red button (<see cref="RedButton"/>) and the light (<see cref="FeedbackLights"/>).
    /// - Destroys all tires
    /// - Resets the state of the game (counters, timer, etc.)
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnResetGame(EventParam e)
    {
        EventManager.TriggerEvent("BlinkLightRed", null);
        EventManager.TriggerEvent("AnimateRedButton", null);
        if (_gameStarted)
        {
            foreach (GameObject tire in _tires)
            {
                Destroy(tire);
            }
            _tires.Clear();

            _countTiresConform = 0;
            _countTiresError = 0;

            _time = 0;
            _gameStarted = false;
            _gameFinished = false;

            EventManager.TriggerEvent("ResetScanner", null);
            EventManager.TriggerEvent("ResetCanvas", null);
        }
    }
}
