using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script attached to the screen on the command center. Displays result when a tire is scanned.
/// </summary>
public class ScreenCtrl : MonoBehaviour
{
    [SerializeField] private Material ConformMat;
    [SerializeField] private Material NonConformMat;
    [SerializeField] private Material NonReadableMat;
    [SerializeField] private Material WaitingMat;

    /// <summary>
    /// Subscribes to events from the scanner (TireScanner/Scannette scripts).
    /// </summary>
    void Start()
    {
        EventManager.StartListening("ConformTire", OnConformTire);
        EventManager.StartListening("NonConformTire", OnNonConformTire);
        EventManager.StartListening("NoReadable", OnNonReadableTire);
    }

    /// <summary>
    /// Displays the Conform material.
    /// </summary>
    /// <param name="e">Unused</param>
    void OnConformTire(EventParam e)
    {
        GetComponent<MeshRenderer>().material = ConformMat;
        StartCoroutine(Display());
    }

    /// <summary>
    /// Displays the Non-conform material.
    /// </summary>
    /// <param name="e">Unused</param>
    void OnNonConformTire(EventParam e)
    {
        GetComponent<MeshRenderer>().material = NonConformMat;
        StartCoroutine(Display());
    }

    /// <summary>
    /// Displays the Non-readable material.
    /// </summary>
    /// <param name="e">Unused</param>
    void OnNonReadableTire(EventParam e)
    {
        GetComponent<MeshRenderer>().material = NonReadableMat;
        StartCoroutine(Display());
    }

    /// <summary>
    /// Waits 2 seconds before resetting the display to "Waiting".
    /// </summary>
    /// <returns></returns>
    private IEnumerator Display()
    {
        yield return new WaitForSeconds(2); // Keep displaying result for 2 seconds
        EventManager.TriggerEvent("EnableTireScanner", null); // Re-enables the scanners (that were disabled during the 2 seconds delay)
        GetComponent<MeshRenderer>().material = WaitingMat;
    }
}
