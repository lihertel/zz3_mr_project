using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script attached to the screen on the wall. Used to display instructions, the timer and results at the end of a game.
/// </summary>
public class ResultsCanvas : MonoBehaviour
{
    /// <summary>
    /// There are 5 Text components in the canvas, each representing a line on the screen.
    /// </summary>
    private Text[] _lines;

    /// <summary>
    /// Retrieves lines and subscribes to some events. Displays instructions on the canvas.
    /// </summary>
    private void Start()
    {
        _lines = transform.GetComponentsInChildren<Text>();

        EventManager.StartListening("UpdateCanvas", OnUpdateCanvas);
        EventManager.StartListening("ResetCanvas", OnResetCanvas);

        OnResetCanvas(null);
    }

    /// <summary>
    /// Replaces the text on each line with strings given as parameters.
    /// </summary>
    /// <param name="e">An EventParamStringArray that holds five strings we want to display on the screen</param>
    private void OnUpdateCanvas(EventParam e)
    {
        EventParamStringArray eventsParams = (EventParamStringArray)e;
        string[] paramLines = eventsParams.Strings;
        for (int i = 0; i < paramLines.Length; i++) // Replaces text on the corresponding line with the new one
        {
            _lines[i].text = paramLines[i];
        }
        for (int i = paramLines.Length; i < _lines.Length; i++) // Removes text on other lines
        {
            _lines[i].text = "";
        }
    }

    /// <summary>
    /// Replaces text on each line with pre-defined instructions.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnResetCanvas(EventParam e)
    {
        string[] strings = { "Instructions", "", "Bouton vert : démarrer", "Bouton rouge : réinitialiser", "" };

        for (int i = 0; i < _lines.Length; i++)
        {
            _lines[i].text = strings[i];
        }
    }
}
