using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sript attached to the light bulb on top of the command center.
/// </summary>
public class FeedbackLights : MonoBehaviour
{
    private Material _greenMaterial;
    private Material _redMaterial;

    private Coroutine _coroutine;

    /// <summary>
    /// Retrieves materials and subscribes to necessary events.
    /// </summary>
    void Start()
    {
        MeshRenderer renderer = GetComponent<MeshRenderer>();
        if (renderer != null)
        {
            _greenMaterial = renderer.materials[1];
            _redMaterial = renderer.materials[2];
        }

        EventManager.StartListening("BlinkLightGreen", OnBlinkLightGreen);
        EventManager.StartListening("BlinkLightRed", OnBlinkLightRed);
    }

    /// <summary>
    /// Triggers the animation of the green light.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnBlinkLightGreen(EventParam e)
    {
        if (_coroutine == null) _coroutine = StartCoroutine(BlinkLight(_greenMaterial));
    }

    /// <summary>
    /// Triggers the animation of the red light.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnBlinkLightRed(EventParam e)
    {
        if (_coroutine == null) _coroutine = StartCoroutine(BlinkLight(_redMaterial));
    }

    /// <summary>
    /// Coroutine that manipulates the Emission property of a material to create a blinking effect.
    /// </summary>
    /// <param name="material">The material to animate</param>
    /// <returns>The current state of the coroutine</returns>
    private IEnumerator BlinkLight(Material material)
    {
        float elapsedTime = 0;

        while (elapsedTime < 3.0f) // The animation lasts for 3 seconds
        {
            material.EnableKeyword("_EMISSION");
            elapsedTime += 0.5f;
            yield return new WaitForSeconds(0.5f);
            material.DisableKeyword("_EMISSION");
            elapsedTime += 0.5f;
            yield return new WaitForSeconds(0.5f);
        }

        _coroutine = null;
    }
}
