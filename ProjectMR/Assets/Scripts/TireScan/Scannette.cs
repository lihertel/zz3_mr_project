using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script attached to the portable scanner.
/// </summary>
public class Scannette : MonoBehaviour
{
    private bool _isEnabled = true;

    private GameObject _parent;
    private Vector3 _initialPosition;

    /// <summary>
    /// Stores the initial position of the scanner so that it goes back to its initial position when the game is reset.
    /// Subscribes to necessary events.
    /// </summary>
    private void Start()
    {
        _parent = GameObject.Find("Scanner");
        _initialPosition = _parent.transform.position;
        EventManager.StartListening("EnableTireScanner", OnEnableScanner);
        EventManager.StartListening("ResetScanner", OnResetScanner);
    }

    /// <summary>
    /// If the scanner is enabled, casts a ray to detect any object in front of the scanner. 
    /// If there is an object and it is a tire, checks its Readable and IsConform properties and triggers the corresponding event.
    /// The event is received by the <see cref="ScreenCtrl"/> script.
    /// </summary>
    private void LateUpdate()
    {
        if (_isEnabled && Physics.Raycast(transform.position, transform.up, out var hitInfo))
        {
            if (hitInfo.collider.gameObject.CompareTag("Tire"))
            {
                if (hitInfo.collider.gameObject.GetComponentInParent<Tire>().Readable || gameObject.CompareTag("Scanner"))
                {
                    if (hitInfo.collider.gameObject.GetComponentInParent<Tire>().IsConform)
                    {
                        EventManager.TriggerEvent("ConformTire", null);
                    }
                    else
                    {
                        EventManager.TriggerEvent("NonConformTire", null);
                    }
                }
                else
                {
                    EventManager.TriggerEvent("DisableTireScanner", null);
                }
            }
        }
    }

    /// <summary>
    /// Enables the scanner.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnEnableScanner(EventParam e)
    {
        _isEnabled = true;
    }

    /// <summary>
    /// When the game is reset. Moves this object to its <see cref="_initialPosition"/>.
    /// </summary>
    /// <param name="e"></param>
    private void OnResetScanner(EventParam e)
    {
        _isEnabled = true;
        _parent.transform.position = _initialPosition;
        _parent.transform.localRotation = Quaternion.identity;
    }
}
