using UnityEngine;

/// <summary>
/// Script attached to the big scanner.
/// </summary>
public class TireScanner : MonoBehaviour
{
    private bool _isEnabled = true;

    private void Start()
    {
        EventManager.StartListening("EnableTireScanner", OnEnableScanner);
        EventManager.StartListening("DisableTireScanner", OnDisableScanner);
    }

    /// <summary>
    /// If the scanner is enabled, casts a ray to detect any object below. 
    /// If there is an object and it is a tire, checks its Readable and IsConform properties and triggers the corresponding event.
    /// The event is received by the <see cref="ScreenCtrl"/> script.
    /// </summary>
    private void LateUpdate()
    {
        if (_isEnabled && Physics.Raycast(transform.position, transform.up, out var hitInfo))
        {
            if (hitInfo.collider.gameObject.CompareTag("Tire"))
            {
                if (hitInfo.collider.gameObject.GetComponentInParent<Tire>().Readable || gameObject.CompareTag("Scanner"))
                {
                    if (hitInfo.collider.gameObject.GetComponentInParent<Tire>().IsConform)
                    {
                        EventManager.TriggerEvent("ConformTire", null);
                    }
                    else
                    {
                        EventManager.TriggerEvent("NonConformTire", null);
                    }
                }
                else
                {
                    EventManager.TriggerEvent("NoReadable", null);
                }
            }
        }
    }

    /// <summary>
    /// Enables the scanner.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnEnableScanner(EventParam e)
    {
        _isEnabled = true;
    }

    /// <summary>
    /// Disables the scanner.
    /// </summary>
    /// <param name="e">Unused</param>
    private void OnDisableScanner(EventParam e)
    {
        _isEnabled = false;
    }

}
