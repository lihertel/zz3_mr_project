using UnityEngine;

/// <summary>
/// Script atatched to the Green dumpster (conform tires).
/// </summary>
public class DumpsterOK : MonoBehaviour
{
    /// <summary>
    /// Detects collision with tires and notifies the GameManager to update the score.
    /// </summary>
    /// <param name="other">The collider which triggered the collision</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Tire") && other.GetComponentInParent<Tire>().IsConform)
        {
            EventManager.TriggerEvent("TireInDumpsterConform", null);
        }
    }

    /// <summary>
    /// Detects when a tire leaves the dumpster and notifies the GameManager.
    /// </summary>
    /// <param name="other">The collider which triggered the collision</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Tire") && other.GetComponentInParent<Tire>().IsConform)
        {
            EventManager.TriggerEvent("TireOutDumpsterConform", null);
        }
    }
}