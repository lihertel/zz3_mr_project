using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script atatched to every each key on the keyboard.
/// The name of each key defines which actino is performe on press.
/// E.g.: the game object is named "a" => the a character is inserted in the typed text.
/// </summary>
public class Key : MonoBehaviour
{
    /// <summary>
    /// Triggered when a key is pressed.
    /// Triggers the event corresponding to the key's name.
    /// </summary>
    public void InsertCharacter()
    {
        // Handle specific keys:
        if (gameObject.name.Equals("space", System.StringComparison.OrdinalIgnoreCase))
            EventManager.TriggerEvent("SpacePressed", null);
        else if (gameObject.name.Equals("enter", System.StringComparison.OrdinalIgnoreCase))
            EventManager.TriggerEvent("EnterPressed", null);
        else if (gameObject.name.Equals("return", System.StringComparison.OrdinalIgnoreCase))
            EventManager.TriggerEvent("ReturnPressed", null);
        // Handle letters:
        else
        {
            EventParamString param = new EventParamString();
            param.Value = gameObject.name;
            EventManager.TriggerEvent("KeyPressed", param);
        }
    }
}
