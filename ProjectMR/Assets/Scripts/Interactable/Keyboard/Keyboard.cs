using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Script attached to the keyboard. Allows the user to enter their name.
/// </summary>
public class Keyboard : MonoBehaviour
{
    /// <summary>
    /// The currently typed text.
    /// </summary>
    public TMP_InputField inputField;

    /// <summary>
    /// Maximum number of characters of the typed text.
    /// </summary>
    const int maxLength = 12;

    /// <summary>
    /// Subscribes to the events Keys are supposed to trigger.
    /// </summary>
    void Start()
    {
        EventManager.StartListening("KeyPressed", InsertChar);
        EventManager.StartListening("EnterPressed", Enter);
        EventManager.StartListening("SpacePressed", InsertSpace);
        EventManager.StartListening("ReturnPressed", DeleteChar);
        EventManager.StartListening("ShowKeyboard", Show);
        ResetText();

        gameObject.SetActive(false);
    }

    /// <summary>
    /// Makes the keyboard visible in the virtual environment.
    /// </summary>
    /// <param name="e">Unused</param>
    private void Show(EventParam e)
    {
        gameObject.SetActive(true);
    }

    /// <summary>
    /// Inserts a character in the <see cref="inputField"/>.
    /// </summary>
    /// <param name="obj">An EventParamString holding the character.</param>
    private void InsertChar(EventParam obj)
    {
        if (inputField.text.Length <= maxLength)
        {
            EventParamString stringEvent = (EventParamString)obj;
            inputField.text += stringEvent.Value;
        }
    }

    /// <summary>
    /// Clears the <see cref="inputField"/>.
    /// </summary>
    public void ResetText()
    {
        inputField.text = "";
    }

    /// <summary>
    /// Removes the last character in the <see cref="inputField"/>.
    /// </summary>
    /// <param name="obj">Unused</param>
    public void DeleteChar(EventParam obj)
    {
        if (inputField.text.Length > 0)
        {
            inputField.text = inputField.text.Substring(0, inputField.text.Length - 1);
        }
    }

    /// <summary>
    /// Inserts a space in the <see cref="inputField"/>.
    /// </summary>
    /// <param name="obj">Unused</param>
    public void InsertSpace(EventParam obj)
    {
        inputField.text += " ";
    }

    /// <summary>
    /// Validates the typed name, then hides the keyboard.
    /// Notifies the GameManager.
    /// </summary>
    /// <param name="obj"></param>
    public void Enter(EventParam obj)
    {
        EventParamString param = new EventParamString();
        param.Value = inputField.text;
        gameObject.SetActive(false);

        EventManager.TriggerEvent("EndGame", param);
    }
}
