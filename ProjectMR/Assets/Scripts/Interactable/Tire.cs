using UnityEngine;

/// <summary>
/// Script attached t oeach tire. Inherits <see cref="Grabbable"/> to work with the laser pointer.
/// </summary>
public class Tire : Grabbable
{
    /// <summary>
    /// True if this tire is conform, false otherwise.
    /// </summary>
    public bool IsConform { get; private set; }

    /// <summary>
    /// True if this tire can be scanned by the main scanner, false otherwise.
    /// If false, the portable scanner must be used.
    /// </summary>
    public bool Readable { get; private set; }

    /// <summary>
    /// Initializes <see cref="IsConform"/> and <see cref="Readable"/> randomly.
    /// </summary>
    protected override void Start()
    {
        base.Start();
        IsConform = (Random.Range(0, 2) == 0);
        Readable = (Random.Range(0, 2) == 1);
    }
}
