using UnityEngine;
using Valve.VR;

/// <summary>
/// Script attached to any object that must be grabbable with the laser pointer (<see cref="PointerManager"/> script).
/// </summary>
public class Grabbable : MonoBehaviour
{
    [SerializeField]
    private SteamVR_Input_Sources trackpad;

    [SerializeField]
    private SteamVR_Input_Sources handtype;

    [SerializeField]
    private SteamVR_Action_Vector2 GrabbedObjectRotate;

    private Rigidbody _rigidbody;

    private bool _isGrabbed = false;

    private GameObject _grabSource = null;

    private bool usePhysics;

    /// <summary>
    /// Subscribes to the Grab event (triggered by the <see cref="PointerManager"/>).
    /// </summary>
    protected virtual void Start()
    {
        _rigidbody = transform.GetComponent<Rigidbody>();
        usePhysics = true;
        EventManager.StartListening("Grab", OnToggleGrab);
    }

    /// <summary>
    /// Allows the user to take the grabbed object away or closer. 
    /// This is controlled via the right controller trackpad.
    /// </summary>
    private void Update()
    {
        if (_isGrabbed)
        {
            if (transform.localPosition.z > 0.5f || GrabbedObjectRotate.GetAxis(trackpad).y > 0) // Prevents the object coming too close to the hand. Avoid conflict with the other grabbing system (SteamVR Interactable).
            {
                transform.position += _grabSource.transform.forward * GrabbedObjectRotate.GetAxis(trackpad).y * 0.025f;
            }
        }
    }

    /// <summary>
    /// Grabs or releases an object.
    /// </summary>
    /// <param name="e">An EventParam that contains a reference to the grabbed object and a reference to the grabber object (usually the hand)</param>
    protected void OnToggleGrab(EventParam e)
    {
        if (e.receiver == gameObject) // Checks if the grabbed object is this (because the event is triggered on all Grabbable objects)
        {
            if (!_isGrabbed) // Grabs the object if not grabbed
            {
                _isGrabbed = true;
                e.receiver.transform.position = e.sender.transform.position + e.sender.transform.forward * 1.5f;
                e.receiver.transform.localEulerAngles = Vector3.zero;
                e.receiver.transform.parent = e.sender.transform; // Makes this a child of the hand so that it follows the movements
                _grabSource = e.sender;
                if (usePhysics)
                {
                    e.receiver.GetComponent<Grabbable>().TogglePhysics();
                }
            }
            else // Releases the object if grabbed
            {
                _isGrabbed = false;
                e.receiver.transform.parent = null;
                e.sender = null;
                if (!usePhysics)
                {
                    e.receiver.GetComponent<Grabbable>().TogglePhysics();
                }
            }
        }
    }

    /// <summary>
    /// Disables physics on this object so that it does not fall when grabbed by the laser, or re-enables it.
    /// </summary>
    public void TogglePhysics()
    {
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.isKinematic = !_rigidbody.isKinematic;
        _rigidbody.useGravity = !_rigidbody.useGravity;
        usePhysics = !usePhysics;
    }


    /// <summary>
    /// When this object is destroyed, unsubscribe to the Grab event.
    /// </summary>
    private void OnDestroy()
    {
        EventManager.StopListening("Grab", OnToggleGrab);
    }

    /// <summary>
    /// Releases this object when it is grabbed with the other grabbing system (SteamVR), to prevent conflicts.
    /// </summary>
    public void StopGrab()
    {
        if (_isGrabbed)
        {
            gameObject.GetComponent<Grabbable>().TogglePhysics();
            _isGrabbed = false;
        }
    }
}
