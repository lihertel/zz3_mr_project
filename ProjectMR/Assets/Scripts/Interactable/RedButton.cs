using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script attached to the red button. Handles animation of the button.
/// </summary>
public class RedButton : MonoBehaviour
{
    private Coroutine _coroutine = null;

    /// <summary>
    /// Subscribes to the "AnimateRedButton" event.
    /// </summary>
    void Start()
    {
        EventManager.StartListening("AnimateRedButton", AnimateButton);
    }

    /// <summary>
    /// Starts the animation of the button.
    /// </summary>
    /// <param name="e">Unused</param>
    private void AnimateButton(EventParam e)
    {
        if (_coroutine == null) _coroutine = StartCoroutine(LerpTranslation(0.5f));
    }

    /// <summary>
    /// Coroutine that animates the button.
    /// </summary>
    /// <param name="duration"></param>
    /// <returns>Current state of the coroutine</returns>
    private IEnumerator LerpTranslation(float duration)
    {
        Vector3 startingPos = transform.position;
        Vector3 finalPos = transform.position - transform.up * 0.025f + transform.right * 0.025f;
        float elapsedTime = 0;

        // Pushes the button inwards
        while (elapsedTime < duration / 2)
        {
            transform.position = Vector3.Lerp(startingPos, finalPos, (elapsedTime / duration));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        // Pushes the button outwards
        while (elapsedTime >= duration / 2 && elapsedTime < duration)
        {
            transform.position = Vector3.Lerp(finalPos, startingPos, (elapsedTime / duration));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        _coroutine = null;
    }
}
