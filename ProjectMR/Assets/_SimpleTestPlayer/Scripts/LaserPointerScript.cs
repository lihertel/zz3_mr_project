using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.Extras;

public class LaserPointerScript : MonoBehaviour
{
    public GameObject holder;
    public GameObject pointer;

    public float thickness = 0.02f;

    public bool addRigidBody = false;

    public Color color;

    // Start is called before the first frame update
    void Start()
    {
        //Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Dumpster"), 0); // Prevents collisions between the pointer ray and dumpster collider
        // to allow grabbing objects that are in the dumpster

        holder = new GameObject();
        holder.transform.parent = this.transform;
        holder.transform.localPosition = Vector3.zero;
        holder.transform.localRotation = Quaternion.identity;

        pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
        pointer.transform.parent = holder.transform;
        pointer.transform.localScale = new Vector3(thickness, thickness, 100f);
        pointer.transform.localPosition = new Vector3(0f, 0f, 50f);
        pointer.transform.localRotation = Quaternion.identity;
        BoxCollider collider = pointer.GetComponent<BoxCollider>();
        if (addRigidBody)
        {
            if (collider)
            {
                collider.isTrigger = true;
            }
            Rigidbody rigidBody = pointer.AddComponent<Rigidbody>();
            rigidBody.isKinematic = true;
        }
        else
        {
            if (collider)
            {
                Object.Destroy(collider);
            }
        }
        Material newMaterial = new Material(Shader.Find("Unlit/Color"));
        newMaterial.SetColor("_Color", color);
        pointer.GetComponent<MeshRenderer>().material = newMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        Ray raycast = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        bool bHit = Physics.Raycast(raycast, out hit);

        if (bHit && Input.GetMouseButtonUp(0))
        {
            PointerEventArgs argsClick = new PointerEventArgs();
            argsClick.distance = hit.distance;
            argsClick.flags = 0;
            argsClick.target = hit.transform;
            PointerClick(argsClick);
        }

        float dist = 100f;
        pointer.transform.localScale = new Vector3(thickness, thickness, dist);
        pointer.GetComponent<MeshRenderer>().material.color = color;
        pointer.transform.localPosition = new Vector3(0f, 0f, dist / 2f);
    }

    public void PointerClick(PointerEventArgs e)
    {

        switch (e.target.tag)
        {
            case "Button":
                switch (e.target.name)
                {
                    case "OK":
                        EventManager.TriggerEvent("StartGame", null);
                        break;
                    case "KO":
                        EventManager.TriggerEvent("ResetGame", null);
                        break;
                }
                break;
            // Grabbable objects
            case "Tire":
            case "Scanner":
            case "Grabbable":
                EventParam p = new EventParam();
                p.sender = gameObject;
                p.receiver = e.target.gameObject;
                EventManager.TriggerEvent("Grab", p);
                break;
            // Keyboard tags
            case "Key":
                Debug.Log("key pressed");
                EventParamString c = new EventParamString();
                c.Value = e.target.name;
                EventManager.TriggerEvent("KeyPressed", c);
                break;

            case "Enter":
                EventManager.TriggerEvent("EnterPressed", null);
                break;

            case "Space":
                EventManager.TriggerEvent("SpacePressed", null);
                break;

            case "Return":
                EventManager.TriggerEvent("ReturnPressed", null);
                break;
        }
    }
}
