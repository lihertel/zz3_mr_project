using UnityEngine;

public class Player : MonoBehaviour
{
    // Whether the forward vector for movement is the one from the player's transform, or the one from the camera
    private enum ForwardVector { Player, Camera };

    // Editor fields
    [SerializeField] private Transform playerCamera = null;
    [SerializeField] private float cameraSensitivity = 3.5f;
    [SerializeField] private float movementSpeed = 6.0f;
    [SerializeField] private ForwardVector forwardVector = ForwardVector.Player;
    [SerializeField] [Range(0.0f, 0.5f)] private float smoothMovementDuration = 0.2f;
    [SerializeField] [Range(0.0f, 0.5f)] private float smoothCameraDuration = 0.1f;
    [SerializeField] private float gravity = -13.0f;
    [SerializeField] private bool hideCursor = true;

    // Components
    private CharacterController _characterController;

    // Private members
    private float _cameraPitch = 0.0f;
    private Vector3 _forwardVector = Vector3.zero;
    private float _velocityY = 0.0f;

    private Vector2 _currentDirection = Vector2.zero;
    private Vector2 _currentDirectionVelocity = Vector2.zero;

    private Vector2 _currentMouseDelta = Vector2.zero;
    private Vector2 _currentMouseDeltaVelocity = Vector2.zero;


    // Start is called before the first frame update
    private void Start()
    {
        _characterController = GetComponent<CharacterController>();

        SwitchForwardVector();
        SwitchCursor();
    }

    // Update is called once per frame
    private void Update()
    {
#if UNITY_EDITOR
        SwitchForwardVector();
        SwitchCursor();
#endif
        UpdateCameraRotation();
        UpdatePlayerPosition();
    }

    // Updates player look direction
    private void UpdateCameraRotation()
    {
        Vector2 targetMouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")); // Gets the current camera movement

        _currentMouseDelta = Vector2.SmoothDamp(_currentMouseDelta, targetMouseDelta, ref _currentMouseDeltaVelocity, smoothCameraDuration);

        _cameraPitch -= _currentMouseDelta.y * cameraSensitivity;       // Substraction (instead of addition) addresses the inverted Y axis problem
        _cameraPitch = Mathf.Clamp(_cameraPitch, -90.0f, 90.0f);        // Prevents camera flipping
        playerCamera.localEulerAngles = Vector3.right * _cameraPitch;   // Rotates the camera along the X axis

        transform.Rotate(Vector3.up, _currentMouseDelta.x * cameraSensitivity); // Rotates the player transform along the Y axis
    }

    // Updates player position
    private void UpdatePlayerPosition()
    {
        Vector2 targetDirection = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")); // Gets the current axix values
        targetDirection.Normalize(); // Prevents vector magnitude exceeding 1 when moving diagonally

        _currentDirection = Vector2.SmoothDamp(_currentDirection, targetDirection, ref _currentDirectionVelocity, smoothMovementDuration);

        // Increases fall down velocity with time
        if (_characterController.isGrounded) _velocityY = 0.0f;
        _velocityY += gravity * Time.deltaTime;

        // Moves the player
        Vector3 velocity = (_forwardVector * _currentDirection.y + transform.right * _currentDirection.x) * movementSpeed + Vector3.up * _velocityY;
        _characterController.Move(velocity * Time.deltaTime);
    }

    // Hides cursor at the center of the screen, according to the choice made in the editor
    private void SwitchCursor()
    {
        switch (hideCursor)
        {
            case true:
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                break;
            case false:
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                break;
        }
    }

    // Sets the value _forwardVector, according to the choice made in the editor
    private void SwitchForwardVector()
    {
        switch (forwardVector)
        {
            case ForwardVector.Player:
                _forwardVector = transform.forward;
                break;
            case ForwardVector.Camera:
                _forwardVector = playerCamera.forward;
                break;
        }
    }
}
